const mongoose = require('mongoose')
const Schema = mongoose.Schema
const productSchema = new Schema({
  name: String,
  price: Number,
  cost: Number
})

const Products = mongoose.model('products', productSchema)

Products.find(function (err, products) {
  if (err) return console.error(err)
  console.log(products)
})

module.exports = mongoose.model('products', productSchema)